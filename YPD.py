import re
import urllib.request
import urllib.error
import sys
import time
from collections import OrderedDict
from pytube import YouTube

def YPD():
    LINKS = list()
    SAVE_PATH = ""
    URL = ""
    RESOLUTION = ""
    VIDEOFORMAT = ""
    def initialize():
        nonlocal SAVE_PATH, URL, RESOLUTION, VIDEOFORMAT
        URL = str(input("Enter Playlist URL: "))
        RESOLUTION = str(input("Enter Video Quality(p): "))
        VIDEOFORMAT = str(input("Enter Video Format: "))
        SAVE_PATH = str(input("Enter Save Directory: "))

    def crawl():
        nonlocal URL, LINKS
        sTUBE = ''
        cPL = ''
        amp = 0
        final_url = []

        if 'list=' in URL:
            eq = URL.rfind('=') + 1
            cPL = URL[eq:]

        else:
            print('Incorrect Playlist.')
            exit(1)

        try:
            yTUBE = urllib.request.urlopen(URL).read()
            sTUBE = str(yTUBE)
        except urllib.error.URLError as e:
            print(e.reason)
        tmp_mat = re.compile(r'watch\?v=\S+?list=' + cPL)
        mat = re.findall(tmp_mat, sTUBE)
        if mat:
            for PL in mat:
                yPL = str(PL)
                if '&' in yPL:
                    yPL_amp = yPL.index('&')
                final_url.append('http://www.youtube.com/' + yPL[:yPL_amp])
            LINKS = list(OrderedDict.fromkeys(final_url))
        else:
            print('No videos found.')
            exit(1)

    def printURLS():
        nonlocal LINKS
        i = 0
        print("List of Videos URL:")
        while i < len(LINKS):
            sys.stdout.write(LINKS[i] + '\n')
            i = i + 1

    def progress_Check(stream = None, chunk = None, file_handle = None, remaining = None):
        percent = (100*(file_size-remaining))/file_size
        print("{:00.0f}% downloaded".format(percent))

    def downloadVideos():
        nonlocal SAVE_PATH, URL, LINKS, RESOLUTION, VIDEOFORMAT
        i = 1
        for l in LINKS:
            get_true = True
            while get_true:
                try:
                    yt = YouTube(l)
                    get_true = False
                except Exception as e:
                    logger.error('Failed to upload to ftp: '+ str(e))
                    print("Connection Error")
                    continue
            try:
                video = yt.streams.first()
                file_name = str(i)+". "+str(video.default_filename)
            except:
                logger.error('Failed to upload to ftp: '+ str(e))
                print("Connection Error")
                exit(1)
            video = yt.streams.filter(file_extension=VIDEOFORMAT, res=RESOLUTION).first() #yt.streams.first() #yt.streams.get(mp4files[-1].extension,mp4files[-1].resolution)
            try:
                print("Starting downloading: "+file_name)
                video.download(SAVE_PATH, filename=file_name)
                i = i+1
            except:
                print("Error, Maybe Duplicate File")
                i = i+1
                continue

    initialize()
    crawl()
    #printURLS()
    downloadVideos()
YPD()
